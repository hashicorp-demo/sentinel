policy "tag-validation" {
    enforcement_level = "hard-mandatory"
}

policy "restrict-quad-zero-sgs" {
    enforcement_level = "soft-mandatory"
}

policy "restrict-azs" {
    enforcement_level = "advisory"
}